# General exports
export FZF_DEFAULT_COMMAND='rg --files'
export EDITOR=nvim
export VISUAL=nvim
export MYVIMRC=~/.vimrc
export VIMCONFIG=~/.vim
export VIMDATA=~/.vim
export PATH=$PATH:$HOME/bin:$VIMCONFIG/pack/minpac/start/fzf/bin

alias sudo='sudo '
alias vi='nvim'
alias vim='nvim $(fzf)'
alias xl='exa --group-directories-first --classify --git'
alias xll='xl -l'
alias ll='exa --group-directories-first --classify --git --all'

export SKIM_DEFAULT_COMMAND="rg --files || find ."
alias skvi='f(){ x="$(sk --bind "ctrl-p:toggle-preview" --ansi --preview="preview.sh -v {}" --preview-window=up:50%:hidden)"; [[ $? -eq 0 ]] && nvim "$x" || true }; f'
alias rgvi='f(){ x="$(sk --bind "ctrl-p:toggle-preview" --ansi -i -c "rg --color=always --line-number \"{}\"" --preview="preview.sh -v {}" --preview-window=up:50%:hidden)"; [[ $? -eq 0 ]] && nvim "$(echo $x|cut -d: -f1)" "+$(echo $x|cut -d: -f2)" || true }; f'
alias skvim='f(){ x="$(sk --bind "ctrl-p:toggle-preview" --ansi --preview="preview.sh -v {}" --preview-window=up:50%:hidden)"; [[ $? -eq 0 ]] && gvim --remote-silent "$x" || true }; f'
alias rgvim='f(){ x="$(sk --bind "ctrl-p:toggle-preview" --ansi -i -c "rg --color=always --line-number \"{}\"" --preview="preview.sh -v {}" --preview-window=up:50%:hidden)"; [[ $? -eq 0 ]] && gvim --remote-silent +":$(echo $x|cut -d: -f2)|" "$(echo $x|cut -d: -f1)" || true }; f'


# OS Detection
UNAME=`uname`

# Fallback info
CURRENT_OS='Linux'

if [[ $UNAME == 'Darwin' ]]; then
    CURRENT_OS='OS X'
else
    eval $(keychain --eval --quiet /home/jbi/.ssh/id_rsa)
    M2_REPO=$HOME/.m2/repository
    PATH=$PATH:/opt/apache-maven-3.6.0/bin:$HOME/.local/bin
    export MAVEN_OPTS='-Xmx4096m'
    export GTK_OVERLAY_SCROLLING=0
    export SWT_GTK3=1
    #export M2_HOME
    export M2_REPO
    export BROWSER=chromium
    #export DOCKER_HOST=tcp://edvjbi.fms.local:2376 DOCKER_TLS_VERIFY=1
    #export DOCKER_IP=10.11.4.92
    export MOZILLA_FIVE_HOME=/usr/bin/firefox
    alias "svnjbi=svn log | sed -n '/jbi/,/-----$/ p'"
fi

# This fixes using SSH in urxvt
# Problem: then solarized colors dont work
#if [[ $TERM == 'rxvt-unicode-256color' ]] ; then
#    export TERM='xterm'
#fi

# History stuff
export HISTSIZE=2000
export SAVEHIST=2000
export HISTFILE=~/.zhistory
setopt inc_append_history
setopt hist_ignore_all_dups
setopt hist_ignore_space

# bind some special keys
bindkey -v
bindkey '^R' history-incremental-search-backward    # Strg+R history-incremental-search-backward
bindkey '^[[2~' overwrite-mode                      # Einfg
bindkey '^[[3~' delete-char                         # Entf
bindkey '^[[A' history-search-backward              # up arrow for back-history-search
bindkey '^[[B' history-search-forward               # down arrow for fwd-history-search
bindkey '^[OH'     beginning-of-line        # Pos1
bindkey '^[OF'     end-of-line              # Ende
bindkey '^[[1;5C'  forward-word             # Strg+right
bindkey '^[[1;5D'  backward-word            # Strg+left

#autoload -U compinit colors
#compinit
#colors
zstyle ':completion:*' menu select

# coloured man pages
man() {
    env LESS_TERMCAP_mb=$'\E[01;31m' \
    LESS_TERMCAP_md=$'\E[01;38;5;74m' \
    LESS_TERMCAP_me=$'\E[0m' \
    LESS_TERMCAP_se=$'\E[0m' \
    LESS_TERMCAP_so=$'\E[38;5;246m' \
    LESS_TERMCAP_ue=$'\E[0m' \
    LESS_TERMCAP_us=$'\E[04;38;5;146m' \
    man "$@"
}


# Load Antigen
source ~/.zsh-antigen/antigen/antigen.zsh

antigen bundle zsh-users/zsh-syntax-highlighting
#antigen theme bhilburn/powerlevel9k powerlevel9k
antigen theme refined
antigen bundle git
antigen bundle zsh-users/zsh-autosuggestions

antigen apply

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/jbi/google-cloud-sdk/path.zsh.inc' ]; then . '/home/jbi/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/home/jbi/google-cloud-sdk/completion.zsh.inc' ]; then . '/home/jbi/google-cloud-sdk/completion.zsh.inc'; fi
