Cureently moving to GitLab
==========================

https://gitlab.com/johannes303


zsh-antigen
===========

An example use is the following::

  mkdir ~/.zsh-antigen
  cd ~/.zsh-antigen
  git submodule update --init antigen

Then clone/update the repository and symlink zsh config file(s).

To update the a package (default is all)::

  antigen update <bundle-name>
